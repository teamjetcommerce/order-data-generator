const fs = require('fs')
const faker = require('faker')

const NUMBER_OF_PRODUCTS = 1e3
const NUMBER_OF_CATEGORIES = 1e2
const NUMBER_OF_PHONES = 1e3
const NUMBER_OF_EMAILS = 1e3
const NUMBER_OF_USERS = 1e3

const arrayOfCount = count => (new Array(count)).join('-').split('-').map((_, i) => i+1)

const getAddress = () => ({
    city: faker.address.city(),
    region: faker.address.state(),
    postcode: faker.address.zipCode(),
    country: faker.address.country(),
    line1: faker.address.streetAddress(),
    line2: faker.address.secondaryAddress()
})

const MAX_NUMBER_OF_PRODUCTS_IN_AN_ORDER = 5
const MIN_NUMBER_OF_PRODUCTS_IN_AN_ORDER = 2


const getRandomNumberBetweenLimits = (min, max) => {
    if (min > max) throw new Error('Min cannot be greater than max')
    const number = Math.floor((Math.random()*max))
    const minNumber = number + min
    return  minNumber > max ? number : minNumber
}

const randomElementFromArray = array => {
    return array[getRandomNumberBetweenLimits(0, array.length)]
}

class Order {

    static randomPaymentMethod() {
        const methods = [
            'Credit Card',
            'Debit Card',
            'Apple Pay',
            'Paypal',
            'Amazon Pay'
        ]
        
        return randomElementFromArray(methods)
    }

    static createOrder(items, status, address) {
        if (!this.lastOrderId) {
            this.lastOrderId = 1
        }

        const amount = items.map((
            { price, quantity }) => price * quantity
        ).reduce(
            (a, b) => a + b,
            0
        )
        const { region, city, country, postcode, line1, line2 } = address

        return {
            order_id: this.lastOrderId++,
            order_status: status,
            amount,
            region: region,
            postcode: postcode,
            country: country,
            city: city,
            address_line_1: line1,
            address_line_2: line2,
            items,
            payment_method: this.randomPaymentMethod()
        }
    }

    static createRandomOrder() {
        const randomItems = arrayOfCount(getRandomNumberBetweenLimits(MIN_NUMBER_OF_PRODUCTS_IN_AN_ORDER, MAX_NUMBER_OF_PRODUCTS_IN_AN_ORDER)).map(Product.random.bind(Product))
        return this.createOrder(randomItems, 'shipped', getAddress())
    }
}

class Product {
    static _setup() {
        if (!this._isInitialized) {
            this._size = NUMBER_OF_PRODUCTS
            this._database = arrayOfCount(this._size).map(this.generateProduct)
            this._isInitialized = true
        }
    }

    static generateProduct(id) {
        const name = faker.commerce.productName()
        const price = parseFloat(faker.commerce.price())

        return {
            id: id.toString(),
            name,
            price,
            categories: Category.randomArray()
        }
    }

    static random() {
        this._setup()
        return Object.assign({}, this._database[faker.random.number(this._size - 1)], { quantity: 1 })
    }

    static getNext() {
        this._setup()
        if (!this.counter) {
            this.counter = 0
        }
        return this._database[this.counter++]
    }
}

class Category {
    static _setup() {
        if (!this._isInitialized) {
            this._size = NUMBER_OF_CATEGORIES
            this._database = arrayOfCount(this._size).map(this.generate)
            this._isInitialized = true
        }
    }

    static generate(id) {
        const name = faker.commerce.productName()

        return {
            id: id.toString(),
            name
        }
    }

    static random() {
        this._setup()
        return this._database[faker.random.number(this._size - 1)]
    }

    static randomArray() {
        this._setup()
        const randomSize = faker.random.number(5) + 1
        return arrayOfCount(randomSize).map(this.random.bind(this))
    }

    static getNext() {
        this._setup()
        if (!this.counter) {
            this.counter = 0
        }
        if (this.counter >= this._size) {
            this.counter = 0
        }
        return this._database[this.counter++]
    }
}

class Email {
    static _setup() {
        if (!this._isInitialized) {
            this._size = NUMBER_OF_EMAILS
            this._emails = arrayOfCount(this._size).map(this.generateEmail)
            this._isInitialized = true
        }
    }

    static generateEmail() {
        return faker.internet.email()
    }

    static random() {
        this._setup()
        return this._emails[faker.random.number(this._size - 1)]
    }

    static get(id) {
        this._setup()
        return this._emails[id]
    }
}

class Phone {
    static _setup() {
        if (!this._isInitialized) {
            this._size = NUMBER_OF_PHONES
            this._phones = arrayOfCount(this._size).map(this.generatePhone)
            this._isInitialized = true
        }
    }

    static generatePhone() {
        return faker.phone.phoneNumberFormat()
    }

    static random() {
        this._setup()
        return this._phones[faker.random.number(this._size - 1)]
    }

    static get(id) {
        this._setup()
        return this._phones[id]
    }
}

class User {
    static _setup() {
        if (!this._isInitialized) {
            this._size = NUMBER_OF_USERS
            this._database = arrayOfCount(this._size).map(this.generateUser)
            this._isInitialized = true
        }
    }

    static generateUser(uid) {
        return {
            uid: uid.toString(),
            first_name: faker.name.firstName(),
            last_name: faker.name.lastName(),
            email: Email.get(uid-1),
            phone: Phone.get(uid-1)
        }
    }

    static random() {
        this._setup()
        return this._database[faker.random.number(this._size - 1)]
    }
}

const generateEvent = id => {
    const user = User.random()
    const order = Order.createRandomOrder()
    return {
        id,
        user,
        ...order
    }
}

const generateAndSaveData = () => {
    const data = arrayOfCount(numberOfEvents).map(generateEvent)
    fs.writeFileSync('./data.json', JSON.stringify(data, null, 4), { encoding: 'utf-8' })
}

let numberOfEvents = 1e3

const arguments = process.argv.slice(2)
const indexOfN = arguments.findIndex(el => el === '--n')
if (indexOfN === -1) {
    console.log('No number(--n) argument found. Default to 1000')
    generateAndSaveData(numberOfEvents)
} else {
    const value = arguments[indexOfN + 1]
    if (!value) {
        throw new Error('Value not found for --n')
    } else {
        const parsedValue = parseInt(value)
        if (!parsedValue) {
            throw new Error('Invalid number found for --n')
        } else {
            numberOfEvents = parsedValue
            if (parsedValue > 1e5) {
                throw new Error('Cannot handle such a great value')
            }
            generateAndSaveData(numberOfEvents)
        }
    }
}