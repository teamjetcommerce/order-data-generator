### Clone the repository, and run 

`npm install`

---

### To generate events of array of length n, run command:- 
`node index.js --n [n]`

---

### This will generate a data.json file in the repository.